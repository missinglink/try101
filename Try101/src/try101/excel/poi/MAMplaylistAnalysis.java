package try101.excel.poi;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;
import java.text.ParseException;
//import javax.servlet.ServletException;

//import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
//import org.apache.poi.poifs.filesystem.*;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;

//import org.apache.log4j.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MAMplaylistAnalysis {
	public static String titleHeader = "排檔節目名稱";
	public static String prgIdHeader = "排檔節目編號";
	public static String episodeHeader = "集別";
	public static String channelHeader = "頻道";
	public static String dateHeader = "播出日期";
	public static String timeHeader = "播出時間";
	public static String durationHeader = "節目時長";
	public static int colTitle = 0;
	public static int colPrgId = 0;
	public static int colEpisode = 0;
	public static int colChannel = 0;
	public static int colDate = 0;
	public static int colTime = 0;
	public static int colDuration = 0;
	
	//private static Logger logger = Logger.getLogger(SCECworkbookUtil.class);
	private static String deptStr = null;
	private static String titleStr = null;
	String sortBy = null;
	

	class ProgramInfo {
		private String id;
		private String title;
		private Date date;
		private int duration;
		private String channel;
		private String episode;
		
		public ProgramInfo() {};

		public ProgramInfo(String id, String title, Date date, int duration, String channel, String episode) {
			this.id = id;
			this.title = title;
			this.date = date;
			this.duration = duration;
			this.channel = channel;
			this.episode = episode;
		}
		
		public String getId() {
			return this.id;
		}
		
		public String getTitle() {
			return this.title;
		}

		public Date getDate() {
			return this.date;
		}
		
		public int getDuration() {
			return this.duration;
		}
		
		public String getChannel() {
			return this.channel;
		}
		
		public String getEpisode() {
			return this.episode;
		}
	}
	
	public ProgramInfo createProgramInfo(String id, String title, Date date, int duration, String channel, String episode) {
		ProgramInfo inner = new ProgramInfo(id, title, date, duration, channel, episode);
		
		return inner;
	}

	
	public static void main(String [] args) throws IOException {
		//Common
		Workbook wb = null, wbx = null;
		CreationHelper createHelper = null;
		Sheet sheet = null;
		Row row = null;
		Cell cell = null;
		CellStyle cellStyle = null;
		NPOIFSFileSystem fs = null;
			
		//Workbook xwb = new XSSFWorkbook();
		String value;
		int rowStart, rowEnd;
		int ex = 19;

		Map.Entry me = null;
		FileOutputStream fOut = null;
		FileOutputStream fileOut = null;

		String outf = "d:/Project/MAM/out.xlsx";
		//InputStream inp = new FileInputStream("c:/Project/MAM/out.xlsx");
		//FileOutputStream fOut = new FileOutputStream("d:/Project/MAM/out.xlsx");
		Workbook wbOut = new XSSFWorkbook();
		Sheet sheetOut = wbOut.createSheet("program");
		Row rOut = null;

		//case 18
		Map<String, Date> hakaMap = new LinkedHashMap<String, Date>();
		Map<String, Date> ptsMap = new LinkedHashMap<String, Date>();
		Map<String, Date> macroMap = new LinkedHashMap<String, Date>();
		List<Map.Entry<String, Date>> hakaList = null, ptsList = null, macroList = null;
		
		//case 19
		Map<String, ProgramInfo> hakaMap1 = new LinkedHashMap<String, ProgramInfo>();
		Map<String, ProgramInfo> ptsMap1 = new LinkedHashMap<String, ProgramInfo>();
		Map<String, ProgramInfo> pts2Map1 = new LinkedHashMap<String, ProgramInfo>();
		Map<String, ProgramInfo> macroMap1 = new LinkedHashMap<String, ProgramInfo>();
		List<Map.Entry<String, ProgramInfo>> hakaList1 = null, ptsList1 = null, pts2List1 = null, macroList1 = null, allChannelList = null;
		ProgramInfo pgm = null;
		MAMplaylistAnalysis mam = new MAMplaylistAnalysis();
		HashMap<String, Integer> programHeader = null;


		Map<String, Date> dimoMap = new LinkedHashMap<String, Date>();
		Map<String, Date> hdMap = new LinkedHashMap<String, Date>();
		Date oDate = null, oTime = null;
		SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd"); 
		SimpleDateFormat tFormat = new SimpleDateFormat("HH:mm:ss"); 
		Date beginDate = null, endDate = null;
		String [] items = null;
		String sName = null, sTitle = null, sDate = null, sTime = null;
		String hh, mm, ss, ff; //ff = frame
		//String [] files = {
			//"c:/Project/MAM/haka-2006-2007.xlsx"
			//,"c:/Project/MAM/haka-2008-2009.xlsx"
			//,"c:/Project/MAM/haka-2010-2011.xlsx"
			//,"c:/Project/MAM/haka-2012-2013.xlsx"
			//,"c:/Project/MAM/haka-2014.xlsx"
		//};
		String [] files = null;
		String patternStr = "\\d\\d:\\d\\d:\\d\\d(:\\d\\d)?";
		//String patternStr = ":+";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = null;
		Cell c = null;
		Set<Map.Entry<String, Date>> set = hakaMap.entrySet();
		Iterator ite = set.iterator();
		
		String sPGMid = null, sEpisode = null, sChannel = null, sID = null;
		
		int date, numOfSheet, event;
		int duration = 0, ihh, imm, iss, iff, hh2s = 60 * 60, mm2s = 60;
		int i = 0, j = 0;

		Comparator<Map.Entry<String, Date>> comparator = new Comparator<Map.Entry<String, Date>>() {

			public int compare(Map.Entry<String, Date> o1, Map.Entry<String, Date> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		};

		Comparator<Map.Entry<String, ProgramInfo>> comparator2 = new Comparator<Map.Entry<String, ProgramInfo>>() {

			public int compare(Map.Entry<String, ProgramInfo> o1, Map.Entry<String, ProgramInfo> o2) {

				return o1.getValue().getDate().compareTo(o2.getValue().getDate());
			}
		};
		
		ex = Integer.valueOf(args[0]);

		switch (ex) {
		case 1:
			wb = new HSSFWorkbook();
			Sheet sheet1 = wb.createSheet("new sheet");
			Sheet sheet2 = wb.createSheet("second sheet");
			fileOut = new FileOutputStream("c:/TEMP/workbook1.xls");
			wb.write(fileOut);
			fileOut.close();
			
			wbx = new XSSFWorkbook();
			sheet1 = wbx.createSheet("new sheet");
			sheet2 = wbx.createSheet("secnod sheet");
			fileOut = new FileOutputStream("c:/TEMP/workbook1.xlsx");
			wbx.write(fileOut);
			fileOut.close();
			break;
		case 2:
			//FileOutputStream fileOut = new FileOutputStream("workbook.xlsx");
			//xwb.write(fileOut);
			//fileOut.close();/
			break;
		case 3:
			wb = new HSSFWorkbook();
			String safeName = WorkbookUtil.createSafeSheetName("[O'Brien's sales*?]"); // returns " O'Brien's sales   "
			Sheet sheet3 = wb.createSheet(safeName);

			fileOut = new FileOutputStream("workbook2.xls");
			wb.write(fileOut);
			fileOut.close();
			break;
		case 7:
			try {
				wb = WorkbookFactory.create(new File("workbook.xls"));
				//wb = WorkbookFactory.create(new File("c:/TEMP/SCEC.xlsx"));
				sheet = wb.getSheetAt(0);
				String sheetName = sheet.getSheetName();
				System.out.println(sheetName);
				row = sheet.getRow(2);
				cell = row.getCell(0);
				
				CellStyle style = cell.getCellStyle();
				int alignment = style.getAlignment(); 
				switch (alignment) {
				case CellStyle.ALIGN_GENERAL:
					System.out.println("ALIGN_GENERAL");
					break;
				case CellStyle.ALIGN_LEFT:
					System.out.println("ALIGN_LEFT");
					break;
				case CellStyle.ALIGN_CENTER:
					System.out.println("ALIGN_CENTER");
					break;
				case CellStyle.ALIGN_RIGHT:
					System.out.println("ALIGN_RIGHT");
					break;
				case CellStyle.ALIGN_FILL:
					System.out.println("ALIGN_FILL");
					break;
				case CellStyle.ALIGN_JUSTIFY:
					System.out.println("ALIGN_JUSTIFY");
					break;
				case CellStyle.ALIGN_CENTER_SELECTION:
					System.out.println("ALIGN_CENTER_SELECTION");
					break;
				default:
					System.out.println("Error:Unknow Align Type");
					break;
				}
				
				String fmStr = style.getDataFormatString();
				System.out.println("Data format string:" + fmStr);
				
				double dvalue = cell.getNumericCellValue();
				System.out.println(dvalue);
			} catch (InvalidFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		case 17:
			files = new String[args.length];
			for (i = 1; i < args.length; i++) {
				files[i - 1] = args[i];
			}

			try {
				beginDate = dFormat.parse("20141001");
				endDate = dFormat.parse("20141031");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			for (i = 0; i < files.length; i++) {
				try {
					wb = WorkbookFactory.create(new File(files[i]));
				} catch (InvalidFormatException e) {
					e.printStackTrace();
				}
				
				//Get number of sheet
				numOfSheet = wb.getNumberOfSheets();
				for (int ns = 0; ns < 1; ns++) {
					sheet = wb.getSheetAt(ns);
					sName = sheet.getSheetName();
					rowStart = sheet.getFirstRowNum();
					rowEnd = sheet.getLastRowNum();
					System.out.println("Row start=" + rowStart);
					System.out.println("Row End=" + rowEnd);
					value = null;
				    // Decide which rows to process
				    //rowStart = Math.min(15, sheet.getFirstRowNum());
				    //rowEnd = Math.max(1400, sheet.getLastRowNum());
		
					//Check header
				    Row r1 = sheet.getRow(rowStart);
				    int firstCol = r1.getFirstCellNum();
				    int lastCol = r1.getLastCellNum();
				    int colIdx = 0, currentCol = 0;
				    
				    for (int rowNum1 = rowStart + 1; rowNum1 <= rowEnd + 1; rowNum1++) {
				        r1 = sheet.getRow(rowNum1);
		
				        if (r1 == null)
				    	   continue;
				        
				        /*
				         * get program ID 
				         */
				        c = r1.getCell(1, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sPGMid = getCellValueStr(c);
				        System.out.printf("(%d,  %d) = %s | ", rowNum1, 5, sPGMid);
				        
				        /*
				         * get program title
				         */
				        c = r1.getCell(1, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sTitle = getCellValueStr(c);
				        System.out.printf("(%d,  %d) = %s | ", rowNum1, 5, sTitle);
				        
		
				        //get play date
				        c = r1.getCell(1, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sDate = getCellValueInt2Str(c);
				        System.out.printf("%s: (%d,  %d) = %s | ", files[i], rowNum1, 1, sDate);
				        try {
				        	oDate = dFormat.parse(sDate);
				        } catch (ParseException e){
				        	e.printStackTrace();
				        	System.out.printf("Error date format!\n");
				        	continue;
				        }
				        
				        /*
				         * get event
				         */
				        /*
				        c = r1.getCell(2, Row.RETURN_BLANK_AS_NULL);
			        	event = getCellValueInt(c);
				         */
				        
				        
			        	/*
			        	 * get play time
			        	 */
				        /*
				        c = r1.getCell(3, Row.RETURN_BLANK_AS_NULL);
			        	sTime = getCellValueStr(c);
			        	items = sTime.split(":");
			        	hh = items[0];
			        	mm = items[1];
			        	ss = items[2];
			        	ff = items[3];
			        	sTime = hh + ":" + mm + ":" + ss;
			        	try {
			        		oTime = tFormat.parse(sTime);
			        	} catch (ParseException e) {
			        		e.printStackTrace();
			        	}
			        	*/
				        
				        		
				        /*
				         * get duration
				         */
				        c = r1.getCell(6, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        if (c.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				        	if (DateUtil.isCellDateFormatted(c)) {
				        		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
				        		sTime  = sdf.format(c.getDateCellValue());
				        	}
				        } else {
				        	c.setCellType(c.CELL_TYPE_STRING);
				        	sTime = getCellValueStr(c);
				        }
				        matcher = pattern.matcher(sTime);
				        if (matcher == null || !matcher.find()) {
				        	System.out.printf("Error time format in %d\n", rowNum1);
				        	continue;
				        }
				        items = sTime.split(":");
				        ihh = Integer.valueOf(items[0]);
				        imm = Integer.valueOf(items[1]);
				        iss = Integer.valueOf(items[2]);
				        //iff = Integer.valueOf(items[3]);
				        duration = ihh * hh2s + imm * mm2s + iss;
				        System.out.printf("(%d,  %d) = %s\n", rowNum1, 6, sTime);
				        //System.out.printf("duration : %d:%d:%d = %d seconds", ihh, imm, iss, duration);
				        
				    	  
				        if (duration > 120) {
				        	if (!hakaMap.containsKey(sTitle))
				        		hakaMap.put(sTitle, oDate);
				        }
				        
				    } //end loop rows
				    
				} //end loop sheets
				
			} // end loop xlsx
			
			output(hakaMap.entrySet(), beginDate, endDate, outf);
			
			break;

		case 18:
			files = new String[args.length];
			for (i = 1; i < args.length; i++) {
				files[i - 1] = args[i];
			}
			try {
				beginDate = dFormat.parse("20141001");
				endDate = dFormat.parse("20141031");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			for (i = 0; i < files.length; i++) {
				try {
					wb = WorkbookFactory.create(new File(files[i]));
				} catch (InvalidFormatException e) {
					e.printStackTrace();
				}
				
				//Get number of sheet
				numOfSheet = wb.getNumberOfSheets();
				for (int ns = 0; ns < 1; ns++) {
					sheet = wb.getSheetAt(ns);
					sName = sheet.getSheetName();
					rowStart = sheet.getFirstRowNum();
					rowEnd = sheet.getLastRowNum();
					System.out.println("Row start=" + rowStart);
					System.out.println("Row End=" + rowEnd);
					value = null;
				    // Decide which rows to process
				    //rowStart = Math.min(15, sheet.getFirstRowNum());
				    //rowEnd = Math.max(1400, sheet.getLastRowNum());
		
					//Check header
				    Row r1 = sheet.getRow(rowStart);
				    int firstCol = r1.getFirstCellNum();
				    int lastCol = r1.getLastCellNum();
				    int colIdx = 0, currentCol = 0;
				    
				    for (int rowNum1 = rowStart + 1; rowNum1 <= rowEnd + 1; rowNum1++) {
				        r1 = sheet.getRow(rowNum1);
		
				        if (r1 == null)
				    	   continue;
				        //int lastColumn = Math.max(r.getLastCellNum(), MY_MINIMUM_COLUMN_COUNT);
				        //firstCol = r1.getFirstCellNum();
				        //lastCol = r1.getLastCellNum();
				        //System.out.printf("\nFirst column = %d, Last column = %d\n", firstCol, lastCol);
		
				        //get program ID
				        c = r1.getCell(0, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sPGMid = getCellValueStr(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 0, sPGMid);
				        
				        /*
				         * get program title
				         */
				        c = r1.getCell(1, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sTitle = getCellValueStr(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 1, sTitle);

				        //get program episode
				        c = r1.getCell(2, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sEpisode = getCellValueStr(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 2, sEpisode);
				        
				        //get play date
				        c = r1.getCell(3, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        oDate = c.getDateCellValue();
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 3, oDate);
				        		
				        /*
				         * get duration
				         */
				        c = r1.getCell(4, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        duration = getCellValueInt(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 4, duration);
				        
				        /*
				         * channel 
				         */
				        c = r1.getCell(5, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sChannel = getCellValueStr(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 5, sChannel);
				        
				        sID = sPGMid + sEpisode;
				        if (sChannel.equals("haka")) {
				        	if (!hakaMap.containsKey(sID)) {
				        		hakaMap.put(sID, oDate);
				        	} else {
				        		System.out.printf("%s : %s %s %s 集 重複\n", sID, sPGMid, sTitle, sEpisode);
				        	}
				        } else if (sChannel.equals("pts")) {
				        	if (!ptsMap.containsKey(sID)) {
				        		ptsMap.put(sID,  oDate);
				        	} else {
				        		System.out.printf("%s : %s %s %s 集 重複\n", sID, sPGMid, sTitle, sEpisode);
				        	}
				        } else if (sChannel.equals("macro")) {
				        	if (!macroMap.containsKey(sID)) {
				        		macroMap.put(sID, oDate);
				        	} else {
				        		System.out.printf("%s : %s %s %s 集 重複\n", sID, sPGMid, sTitle, sEpisode);
				        	}
				        } else {
				        	System.out.printf("Error : got wrong channel\n");
				        }
				    } //end loop rows
				} //end loop sheets
			} // end loop xlsx

			//sorting
			hakaList = new ArrayList<Map.Entry<String, Date>>(hakaMap.entrySet());
			Collections.sort(hakaList, comparator);
			ptsList = new ArrayList<Map.Entry<String, Date>>(ptsMap.entrySet());
			Collections.sort(ptsList, comparator);
			macroList = new ArrayList<Map.Entry<String, Date>>(macroMap.entrySet());
			Collections.sort(macroList, comparator);
			
			
			
			output(hakaList, beginDate, endDate, outf);
			output(ptsList, beginDate, endDate, "d:/Project/MAM/out2.xlsx");
			output(macroList, beginDate, endDate, "d:/Project/MAM/out3.xlsx");
			
			break;

		/*
		 * input : 節目管理系統 4頻道節目表
		 * output : 首播節目統計
		 */
		case 19:
			files = new String[args.length];
			for (i = 1; i < args.length; i++) {
				files[i - 1] = args[i];
			}

			try {
				beginDate = dFormat.parse("20141001");
				endDate = dFormat.parse("20141031");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			for (i = 0; i < files.length; i++) {
				if (files[i] == null)
					continue;

				try {
					wb = WorkbookFactory.create(new File(files[i]));
				} catch (InvalidFormatException e) {
					e.printStackTrace();
				}
				
				//Get number of sheet
				numOfSheet = wb.getNumberOfSheets();
				for (int ns = 0; ns < numOfSheet; ns++) {
					sheet = wb.getSheetAt(ns);
					sName = sheet.getSheetName();
					rowStart = sheet.getFirstRowNum();
					rowEnd = sheet.getLastRowNum();
					System.out.println("Row start=" + rowStart);
					System.out.println("Row End=" + rowEnd);
					value = null;
				    // Decide which rows to process
				    //rowStart = Math.min(15, sheet.getFirstRowNum());
				    //rowEnd = Math.max(1400, sheet.getLastRowNum());
		
					//Check header
				    Row r1 = sheet.getRow(rowStart);
				    int firstCol = r1.getFirstCellNum();
				    int lastCol = r1.getLastCellNum();
				    int colIdx = 0, currentCol = 0;
				    
				    /*
				     * reading header
				     */
				    programHeader = readProgramHeader(sheet);
				    //排檔節目名稱
				    colTitle = programHeader.get(titleHeader);
				    //排檔節目編號
				    colPrgId = programHeader.get(prgIdHeader);
				    //集別
				    colEpisode  = programHeader.get(episodeHeader);
				    //頻道
				    colChannel = programHeader.get(channelHeader);
				    //播出日期
				    colDate = programHeader.get(dateHeader);
				    //colTime = programHeader.get(timeHeader);
				    //播出時長
				    colDuration = programHeader.get(durationHeader);
				    
				    for (int rowNum1 = rowStart + 1; rowNum1 <= rowEnd + 1; rowNum1++) {
				        r1 = sheet.getRow(rowNum1);
		
				        if (r1 == null)
				    	   continue;
				        //int lastColumn = Math.max(r.getLastCellNum(), MY_MINIMUM_COLUMN_COUNT);
				        //firstCol = r1.getFirstCellNum();
				        //lastCol = r1.getLastCellNum();
				        //System.out.printf("\nFirst column = %d, Last column = %d\n", firstCol, lastCol);
		
				        //get program ID
				        c = r1.getCell(colPrgId, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sPGMid = getCellValueStr(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 0, sPGMid);
				        
				        /*
				         * get program title
				         */
				        c = r1.getCell(colTitle, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sTitle = getCellValueStr(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 1, sTitle);

				        //get program episode
				        c = r1.getCell(colEpisode, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sEpisode = getCellValueStr(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 2, sEpisode);
				        
				        //get play date
				        c = r1.getCell(colDate, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        oDate = c.getDateCellValue();
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 3, oDate);
				        		
				        /*
				         * get duration
				         */
				        c = r1.getCell(colDuration, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        duration = getCellValueInt(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 4, duration);
				        
				        /*
				         * channel 
				         */
				        c = r1.getCell(colChannel, Row.RETURN_BLANK_AS_NULL);
				        if (c == null)
				        	continue;
				        sChannel = getCellValueStr(c);
				        //System.out.printf("(%d,  %d) = %s | ", rowNum1, 5, sChannel);
				        
				        pgm = mam.createProgramInfo(sPGMid, sTitle, oDate, duration, sChannel, sEpisode);
				        sID = sPGMid + "-" + sEpisode;
				        if (sChannel.equalsIgnoreCase("haka")) {
				        	if (!hakaMap1.containsKey(sID)) {
				        		hakaMap1.put(sID, pgm);
				        	} else {
				        		System.out.printf("%s : %s %s %s 集 重複\n", sID, sPGMid, sTitle, sEpisode);
				        	}
				        } else if (sChannel.equalsIgnoreCase("pts")) {
				        	if (!ptsMap1.containsKey(sID)) {
				        		ptsMap1.put(sID, pgm);
				        	} else {
				        		System.out.printf("%s : %s %s %s 集 重複\n", sID, sPGMid, sTitle, sEpisode);
				        	}
				        } else if (sChannel.equalsIgnoreCase("pts2")) {
				        	if (!pts2Map1.containsKey(sID)) {
				        		pts2Map1.put(sID, pgm);
				        	} else {
				        		System.out.printf("%s : %s %s %s 集 重複\n", sID, sPGMid, sTitle, sEpisode);
				        	}
				        } else if (sChannel.equalsIgnoreCase("macro")) {
				        	if (!macroMap1.containsKey(sID)) {
				        		macroMap1.put(sID, pgm);
				        	} else {
				        		System.out.printf("%s : %s %s %s 集 重複\n", sID, sPGMid, sTitle, sEpisode);
				        	}
				        } else {
				        	System.out.printf("Error : got wrong channel\n");
				        }
				    } //end loop rows
				} //end loop sheets
			} // end loop xlsx

			//sorting
			hakaList1 = new ArrayList<Map.Entry<String, ProgramInfo>>(hakaMap1.entrySet());
			Collections.sort(hakaList1, comparator2);
			ptsList1 = new ArrayList<Map.Entry<String, ProgramInfo>>(ptsMap1.entrySet());
			Collections.sort(ptsList1, comparator2);
			pts2List1 = new ArrayList<Map.Entry<String, ProgramInfo>>(pts2Map1.entrySet());
			Collections.sort(pts2List1, comparator2);
			macroList1 = new ArrayList<Map.Entry<String, ProgramInfo>>(macroMap1.entrySet());
			Collections.sort(macroList1, comparator2);
			
			if (hakaList1.size() > 0)
				output1(hakaList1, beginDate, endDate, outf);
			
			if (ptsList1.size() > 0)
				output1(ptsList1, beginDate, endDate, "d:/Project/MAM/out2.xlsx");
			
			if (macroList1.size() > 0)
				output1(macroList1, beginDate, endDate, "d:/Project/MAM/out3.xlsx");
			
			allChannelList = new ArrayList<Map.Entry<String, ProgramInfo>>();
			for (Map.Entry<String, ProgramInfo> e : hakaList1)
				compareProgram(allChannelList, e);
			//output1(allChannelList, beginDate, endDate, "d:/Project/MAM/all0.xlsx");

			for (Map.Entry<String, ProgramInfo> e : ptsList1)
				compareProgram(allChannelList, e);
			//output1(allChannelList, beginDate, endDate, "d:/Project/MAM/all1.xlsx");

			for (Map.Entry<String, ProgramInfo> e : pts2List1)
				compareProgram(allChannelList, e);
			//output1(allChannelList, beginDate, endDate, "d:/Project/MAM/all2.xlsx");

			for (Map.Entry<String, ProgramInfo> e : macroList1)
				compareProgram(allChannelList, e);

			output1(allChannelList, beginDate, endDate, "d:/Project/MAM/all.xlsx");
			
			break;
		}
	}
	
	public static void compareProgram(List<Map.Entry<String, ProgramInfo>> list, Map.Entry<String, ProgramInfo> current) {
		String id = null, cid = current.getKey(), pgmid = null;
		ProgramInfo pgm = null, pgmc = current.getValue();
		Date datec = null, date = null;
		int i = 0, insertIndex = list.size();
		boolean insert = true;
		
		for (Map.Entry<String, ProgramInfo> e : list) {
			id = e.getKey();
			pgm = e.getValue();
			date = pgm.getDate();
			datec = pgmc.getDate();
			
			if (datec.equals(date))
				insertIndex = i; //record the index to be insert into list
			
			if (id.equals(cid)) { //found the same program
				if (date.after(pgmc.getDate())) { //remove this program then insert pgm in previous date
					list.remove(e); //remove duplicate program
					System.out.printf("insert %s to %s\n", pgmc.title, pgmc.date);
				} else {
					insert = false;
					System.out.printf("%s(%s) : %s is filed on %s in channel %s\n", 
							pgmc.channel, cid, pgmc.title, pgm.date, pgm.channel);
				}
				break;
			}
			i++;
		}
		
		//fix bug : crash if the date being inserted is not in list => array index out of bound, since one entry has been removed from list
		if (insert) {
			if (insertIndex < list.size()) 
				list.add(insertIndex, current);
			else { //find insertIndex by date
				i = 0;
				for (Map.Entry<String, ProgramInfo> e : list) {
					pgm = e.getValue();
					date = pgm.getDate();
					if (date.after(datec))
						break; // find insert position
					i++;
				}
				list.add(i, current);
			}
		}
	}

	public static void output1(List<Map.Entry<String, ProgramInfo>> list, Date beginDate, Date endDate, String f) throws IOException {
		Map.Entry me = null;
		FileOutputStream fileOut = null;
		String sTitle = null, sDate = null;
		Date oDate = null, prevDate = null;
		Row rOut = null;
		Cell cell = null;
		Workbook wbOut = new XSSFWorkbook();
		Sheet sheetOut = wbOut.createSheet("program");
		Sheet sheetDaily = wbOut.createSheet("dailyReport");
		FileOutputStream fOut = new FileOutputStream(f);
		ProgramInfo pgm = null;
		String id = null, title = null, channel = null, episode = null;
		int j = 1, duration = 0, k = 1;
		double dailyHr = 0, total = 0;
				
		rOut = sheetOut.createRow(0);
		cell = rOut.createCell(0);
		cell.setCellValue("排檔節目編號");
		cell = rOut.createCell(1);
		cell.setCellValue("排檔節目名稱");
		cell = rOut.createCell(2);
		cell.setCellValue("集別");
		cell = rOut.createCell(3);
		cell.setCellValue("播出日期");
		cell = rOut.createCell(4);
		cell.setCellValue("實際播出時長");
		cell = rOut.createCell(5);
		cell.setCellValue("頻道");

		rOut = sheetDaily.createRow(0);
		cell = rOut.createCell(0);
		cell.setCellValue("播出日期");
		cell = rOut.createCell(1);
		cell.setCellValue("播出時數(hr)");

		pgm = list.get(0).getValue();
		prevDate = pgm.date;
		for (Map.Entry<String, ProgramInfo> e : list) {
			pgm = e.getValue();
			id = pgm.getId();
			title = pgm.getTitle();
			oDate = pgm.getDate();
			sDate = oDate.toString();
			duration = pgm.getDuration();
			channel = pgm.getChannel();
			episode = pgm.getEpisode();
			
			rOut = sheetOut.createRow(j);
			cell = rOut.createCell(0);
			if (cell == null)
				rOut.createCell(0);
			cell.setCellValue(id);

			cell = rOut.createCell(1);
			if (cell == null)
				rOut.createCell(1);
			cell.setCellValue(title);

			cell = rOut.createCell(2);
			if (cell == null)
				rOut.createCell(2);
			cell.setCellValue(episode);
			
			cell = rOut.createCell(3);
			if (cell == null)
				rOut.createCell(3);
			cell.setCellValue(sDate);

			cell = rOut.createCell(4);
			if (cell == null)
				rOut.createCell(4);
			cell.setCellValue(duration);

			cell = rOut.createCell(5);
			if (cell == null)
				rOut.createCell(5);
			cell.setCellValue(channel);
			
			if (oDate.equals(prevDate)) {
				dailyHr += duration;
			} else {
				rOut = sheetDaily.createRow(k++);
				cell = rOut.createCell(0);
				if (cell == null)
					rOut.createCell(0);
				sDate = prevDate.toString();
				cell.setCellValue(sDate);
				cell = rOut.createCell(1);
				if (cell == null)
					rOut.createCell(1);
				cell.setCellValue(dailyHr / 60);

				dailyHr = duration;
			}
			j++;
			total += duration;
			prevDate = oDate;
		}
		rOut = sheetDaily.createRow(k++);
		cell = rOut.createCell(0);
		if (cell == null)
			rOut.createCell(0);
		sDate = prevDate.toString();
		cell.setCellValue(sDate);
		cell = rOut.createCell(1);
		if (cell == null)
			rOut.createCell(1);
		cell.setCellValue(dailyHr / 60);

		rOut = sheetDaily.createRow(k);
		cell = rOut.createCell(0);
		cell.setCellValue("總播出時數");
		cell = rOut.createCell(1);
		cell.setCellValue(total / 60);

		wbOut.write(fOut);
		fOut.close();
	}

	public static void output(List<Map.Entry<String, Date>> list, Date beginDate, Date endDate, String f) throws IOException {
		Map.Entry me = null;
		FileOutputStream fileOut = null;
		String sTitle = null, sDate = null;
		Date oDate = null;
		Row rOut = null;
		Cell cell = null;
		Workbook wbOut = new XSSFWorkbook();
		Sheet sheetOut = wbOut.createSheet("program");
		FileOutputStream fOut = new FileOutputStream(f);
		int j = 0;
				
		for (Map.Entry<String, Date> e : list) {
			sTitle = e.getKey();
			oDate = e.getValue();
			sDate = oDate.toString();
			rOut = sheetOut.createRow(j);
			cell = rOut.createCell(0);
			if (cell == null)
				rOut.createCell(0);
			cell.setCellValue(sTitle);
			
			cell = rOut.createCell(1);
			if (cell == null)
				rOut.createCell(1);
			cell.setCellValue(sDate);
			j++;
		}

		wbOut.write(fOut);
		fOut.close();
	}

	public static void output(Set<Map.Entry<String, Date>> set, Date beginDate, Date endDate, String f) throws IOException {
		Map.Entry me = null;
		FileOutputStream fileOut = null;
		Iterator ite = set.iterator();
		String sTitle = null, sDate = null;
		Date oDate = null;
		Row rOut = null;
		Cell cell = null;
		Workbook wbOut = new XSSFWorkbook();
		Sheet sheetOut = wbOut.createSheet("program");
		FileOutputStream fOut = new FileOutputStream(f);
		int j = 0;
				

		while (ite.hasNext()) {
			me = (Map.Entry)ite.next();
			sTitle = (String)me.getKey();
			oDate = (Date)me.getValue();
			//rOut = sheetOut.getRow(j);
			//Check date range
			if (oDate.before(beginDate) || oDate.after(endDate))
				continue;
			rOut = sheetOut.createRow(j);
			//cell = rOut.getCell(0);
			cell = rOut.createCell(0);
			if (cell == null)
				rOut.createCell(0);
			//cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue(sTitle);
			
			sDate = oDate.toString();
			//cell = rOut.getCell(1);
			cell = rOut.createCell(1);
			if (cell == null)
				rOut.createCell(1);
			//cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue(sDate);
			j++;
		}
		wbOut.write(fOut);
		fOut.close();
	}
	
	public static String loadExcel(File xlsf) throws RuntimeException {
		Workbook wb = null;
		Sheet sheet = null;
		String sName = null;
		int numOfSheet = 0;
		File logf = new File("/Users/MBP/Documents/out.log");
		FileOutputStream fs = null;
		DataOutputStream df = null;
		//SCECstatus.status = SCECstatusEnum.SCEC_LOAD_EXCEL;
		String encoding = System.getProperty("file.encoding");
		boolean res = true;
		String errMsg = null;
		
		try {
			wb = WorkbookFactory.create(xlsf);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			numOfSheet = wb.getNumberOfSheets();
			
			for (int ns = 0; ns < numOfSheet; ns++) {
				sheet = wb.getSheetAt(ns);
				sName = sheet.getSheetName();
			}
		} catch (RuntimeException r) {
			//TODO:should log this information
			r.printStackTrace();
			throw new RuntimeException(r.getMessage()
										+ "<br/> Sheet : " + SCECmessage.sheet
										+ "<br/> Cell : " + SCECmessage.location);
		} finally {
			try {
				fs = new FileOutputStream(logf);
				df = new DataOutputStream(fs);
				for (String msg : SCECmessage.log) {
					df.writeUTF(msg);
				}
				SCECmessage.log.clear();
				df.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return errMsg;
	}
	
	
	
	private static HashMap<String, Integer> readHeader(Sheet sheet) {
		HashMap<String, Integer> header = new LinkedHashMap<String, Integer>();
		int rowStart = sheet.getFirstRowNum();
		String value = null;
		Row row = null;
		Cell cell = null;

	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    for (colIdx = firstCol; colIdx < lastCol; colIdx++) {
	    	cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
	    	
	        if (cell == null)
	        	continue;
	        
	        value = getCellValueStr(cell);
	        //To get first column index of dept
	        if (value.equals(deptStr)) {
	        	if (header.containsKey(deptStr))
	        		continue;
	        }
	        if (value.equals(titleStr)) {
	        	if (header.containsKey(titleStr))
	        		continue;
	        }
	        currentCol = cell.getColumnIndex();
	        header.put(value, currentCol);
	    }
		
		return header;
	}

	private static HashMap<String, Integer> readProgramHeader(Sheet sheet) {
		HashMap<String, Integer> header = new LinkedHashMap<String, Integer>();
		int rowStart = sheet.getFirstRowNum();
		String value = null;
		Row row = null;
		Cell cell = null;

	    row = sheet.getRow(rowStart);
	    int firstCol = row.getFirstCellNum();
	    int lastCol = row.getLastCellNum();
	    int colIdx = 0, currentCol = 0;
	    for (colIdx = firstCol; colIdx < lastCol; colIdx++) {
	    	cell = row.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
	    	
	        if (cell == null)
	        	continue;
	        
	        value = getCellValueStr(cell);
	        if (!(value.equals(titleHeader)
	        	| value.equals(prgIdHeader)
	        	| value.equals(episodeHeader)
	        	| value.equals(channelHeader)
	        	| value.equals(dateHeader)
	        	| value.equals(timeHeader)
	        	| value.equals(durationHeader)))
	        	continue;
	        	
	        currentCol = cell.getColumnIndex();
	        header.put(value, currentCol);
	    }
		
		return header;
	}

	private static void readColumn(Sheet sheet, int colIdx) {
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		Row row = null;
		Cell cell = null;
		
		for (int r = firstRow + 1; r < lastRow; r++) {
			row = sheet.getRow(r);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null)
				continue;
			
			printCellValue(cell);
		}
	}
	
    /**
     * Creates a cell and aligns it a certain way.
     *
     * @param wb     the workbook
     * @param row    the row to create the cell in
     * @param column the column number to create the cell in
     * @param halign the horizontal alignment for the cell.
     */
    private static void createCell(Workbook wb, Row row, short column, short halign, short valign) {
        Cell cell = row.createCell(column);
        cell.setCellValue("Align It");
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(halign);
        cellStyle.setVerticalAlignment(valign);
        cell.setCellStyle(cellStyle);
    }
    
    private static short getAllignmentType(Cell cell) {
    	CellStyle style = cell.getCellStyle();
    	short type = style.getAlignment();
    	switch(type) {
		case CellStyle.ALIGN_GENERAL:
			System.out.println("ALIGN_GENERAL");
			break;
		case CellStyle.ALIGN_LEFT:
			System.out.println("ALIGN_LEFT");
			break;
		case CellStyle.ALIGN_CENTER:
			System.out.println("ALIGN_CENTER");
			break;
		case CellStyle.ALIGN_RIGHT:
			System.out.println("ALIGN_RIGHT");
			break;
		case CellStyle.ALIGN_FILL:
			System.out.println("ALIGN_FILL");
			break;
		case CellStyle.ALIGN_JUSTIFY:
			System.out.println("ALIGN_JUSTIFY");
			break;
		case CellStyle.ALIGN_CENTER_SELECTION:
			System.out.println("ALIGN_CENTER_SELECTION");
			break;
		default:
			System.out.println("Error:Unknow Align Type");
			type = -1;
			break;
		}
    	
    	return type;
    }
    
    private static int getCellType(Cell cell) {
    	int type = cell.getCellType();
    	switch(type) {
		case Cell.CELL_TYPE_BLANK:
			System.out.println("ALIGN_GENERAL");
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			System.out.println("ALIGN_LEFT");
			break;
		case Cell.CELL_TYPE_ERROR:
			System.out.println("ALIGN_CENTER");
			break;
		case Cell.CELL_TYPE_FORMULA:
			System.out.println("ALIGN_RIGHT");
			break;
		case Cell.CELL_TYPE_NUMERIC:
			System.out.println("ALIGN_FILL");
			break;
		case Cell.CELL_TYPE_STRING:
			System.out.println("ALIGN_JUSTIFY");
			break;
		default:
			System.out.println("Error:Unknow Cell Type");
			type = -1;
			break;
		}
    	
    	return type;
    }
    
    private static String getCellValueStr(Cell cell) {
    	int type = -1;
    	String value = null;
    	
    	if (cell == null) 
    		value = "";
    	else
    		type = cell.getCellType();
    	
    	switch(type) {
		case Cell.CELL_TYPE_BLANK:
			value = "Blank";
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			Boolean boVal = cell.getBooleanCellValue();
			value = boVal.toString();
			break;
		case Cell.CELL_TYPE_ERROR:
			Byte btVal = cell.getErrorCellValue();
			value = btVal.toString();
			break;
		case Cell.CELL_TYPE_FORMULA:
			value = cell.getCellFormula();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			Double dVal = cell.getNumericCellValue();
			value = dVal.toString();
			break;
		case Cell.CELL_TYPE_STRING:
			value = cell.getStringCellValue();
			value = value.trim();
			break;
		default:
			type = -1;
			break;
		}
		//System.out.printf("Value = %s\n", value);
    	
    	return value;
    }
    
    private static double getCellValueDouble(Cell cell) {
    	int type = -1;
    	double value = 0;
    	
    	if (cell != null)
    		type = cell.getCellType();
    	
    	switch(type) {
		case Cell.CELL_TYPE_NUMERIC:
			value = cell.getNumericCellValue();
			break;
		default:
			break;
		}
    	
    	return value;
    }
    
    private static int getCellValueInt(Cell cell) {
    	String sValue = null;
    	int value = 0;
    	
    	cell.setCellType(Cell.CELL_TYPE_STRING);
    	
    	sValue = cell.getStringCellValue();
		value = Integer.valueOf(sValue);
    	
    	return value;
    }
    
    private static String getCellValueInt2Str(Cell cell) {
    	String res = null;
    	int type = -1;
    	cell.setCellType(Cell.CELL_TYPE_STRING);
    	
    	res = cell.getStringCellValue();
    	
    	return res;
    }
    
    private static void printCellValue(Cell cell) {
    	int type = -1;
    	
    	if (cell != null)
    		type = cell.getCellType();
    	
        CellReference cellRef = new CellReference(cell.getRowIndex(), cell.getColumnIndex());
        System.out.print(cellRef.formatAsString());
        System.out.print("=");

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                System.out.printf("%s", cell.getRichStringCellValue().getString());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    System.out.printf("%s", cell.getDateCellValue());
                } else {
                    System.out.printf("%s", cell.getNumericCellValue());
                }
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                System.out.printf("%s", cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA:
                System.out.printf("%s", cell.getCellFormula());
                break;
            default:
                System.out.printf("Error:unknow cell value type");
        }
    }
    
    private static boolean isRowEmpty(Row row) {
    	int first = -1, last = -1;
    	
    	if (row == null)
    		return true;
    	
    	first = row.getFirstCellNum();
    	last = row.getLastCellNum();
    	
    	if (first == -1 || last == -1)
    		return true;
    	
        for (int c = first; c <= last; c++) {
            Cell cell = row.getCell(c);
            if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
                return false;
        }
        return true;
    }
}
