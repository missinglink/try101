package try101.excel.poi;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.sun.rowset.internal.Row;

//test
//test
//test3
//test4
//test5
//test6

public class ReadWrite {

	public static void main(String args[]) {
		InputStream inp = new FileInputStream("workbook.xls");
		//InputStream inp = new FileInputStream("workbook.xlsx");
		
		Workbook wb = WorkbookFactory.create(inp);
		Sheet sheet = wb.getSheetAt(0);
		Row row = (Row) sheet.getRow(2);
		Cell cell =  row.getCell(3);
		if (cell == null)
		    cell = row.createCell(3);
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue("a test");
		
		// Write the output to a file
		FileOutputStream fileOut = new FileOutputStream("workbook.xls");
		wb.write(fileOut);
		fileOut.close();
	}
}
