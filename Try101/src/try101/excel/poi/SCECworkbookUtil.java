package try101.excel.poi;

import java.io.*;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
//import org.apache.poi.poifs.filesystem.*;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;

public class SCECworkbookUtil {
	
	public static void main(String [] args) throws IOException {
		int ex = 16;
		Workbook wb = null, wbx = null;
		FileOutputStream fileOut = null;
		CreationHelper createHelper = null;
		Sheet sheet = null;
		Row row = null;
		Cell cell = null;
		CellStyle cellStyle = null;
		NPOIFSFileSystem fs = null;
		//Workbook xwb = new XSSFWorkbook();

		switch (ex) {
		case 1:
			wb = new HSSFWorkbook();
			Sheet sheet1 = wb.createSheet("new sheet");
			Sheet sheet2 = wb.createSheet("second sheet");
			fileOut = new FileOutputStream("c:/TEMP/workbook1.xls");
			wb.write(fileOut);
			fileOut.close();
			
			wbx = new XSSFWorkbook();
			sheet1 = wbx.createSheet("new sheet");
			sheet2 = wbx.createSheet("secnod sheet");
			fileOut = new FileOutputStream("c:/TEMP/workbook1.xlsx");
			wbx.write(fileOut);
			fileOut.close();
			break;
		case 2:
			//FileOutputStream fileOut = new FileOutputStream("workbook.xlsx");
			//xwb.write(fileOut);
			//fileOut.close();/
			break;
		case 3:
			wb = new HSSFWorkbook();
			String safeName = WorkbookUtil.createSafeSheetName("[O'Brien's sales*?]"); // returns " O'Brien's sales   "
			Sheet sheet3 = wb.createSheet(safeName);

			fileOut = new FileOutputStream("workbook2.xls");
			wb.write(fileOut);
			fileOut.close();
			break;
		case 4:
			//Creating cell
			wb = new HSSFWorkbook();
			createHelper = wb.getCreationHelper();
		    sheet = wb.createSheet("new sheet");

		    // Create a row and put some cells in it. Rows are 0 based.
		    row = sheet.createRow((short)0);
		    // Create a cell and put a value in it.
		    cell = row.createCell(0);
		    cell.setCellValue(1);

		    // Or do it on one line.
		    row.createCell(1).setCellValue(1.2);
		    row.createCell(2).setCellValue(
		    createHelper.createRichTextString("This is a string"));
		    row.createCell(3).setCellValue(true);

		    // Write the output to a file
		    fileOut = new FileOutputStream("workbook.xls");
		    wb.write(fileOut);
		    fileOut.close();
		case 5:
			//Creating Date cell
		    wb = new HSSFWorkbook();
		    //Workbook wb = new XSSFWorkbook();
		    createHelper = wb.getCreationHelper();
		    sheet = wb.createSheet("new sheet");

		    // Create a row and put some cells in it. Rows are 0 based.
		    row = sheet.createRow(0);

		    // Create a cell and put a date value in it.  The first cell is not styled
		    // as a date.
		    cell = row.createCell(0);
		    cell.setCellValue(new Date());

		    // we style the second cell as a date (and time).  It is important to
		    // create a new cell style from the workbook otherwise you can end up
		    // modifying the built in style and effecting not only this cell but other cells.
		    cellStyle = wb.createCellStyle();
		    cellStyle.setDataFormat(
		    createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
		    cell = row.createCell(1);
		    cell.setCellValue(new Date());
		    cell.setCellStyle(cellStyle);

		    //you can also set date as java.util.Calendar
		    cell = row.createCell(2);
		    cell.setCellValue(Calendar.getInstance());
		    cell.setCellStyle(cellStyle);

		    // Write the output to a file
		    fileOut = new FileOutputStream("workbook.xls");
		    wb.write(fileOut);
		    fileOut.close();
		case 6:
			//Working with different types of cells
		    wb = new HSSFWorkbook();
		    sheet = wb.createSheet("new sheet");
		    row = sheet.createRow((short)2);
		    row.createCell(0).setCellValue(1.1);
		    row.createCell(1).setCellValue(new Date());
		    row.createCell(2).setCellValue(Calendar.getInstance());
		    row.createCell(3).setCellValue("a string");
		    row.createCell(4).setCellValue(true);
		    row.createCell(5).setCellType(Cell.CELL_TYPE_ERROR);

		    // Write the output to a file
		    fileOut = new FileOutputStream("workbook.xls");
		    wb.write(fileOut);
		    fileOut.close();
		    break;
		case 7:
			//File vs InputStream
			// Use a file
			try {
				wb = WorkbookFactory.create(new File("workbook.xls"));
				//wb = WorkbookFactory.create(new File("c:/TEMP/SCEC.xlsx"));
				sheet = wb.getSheetAt(0);
				String sheetName = sheet.getSheetName();
				System.out.println(sheetName);
				row = sheet.getRow(2);
				cell = row.getCell(0);
				
				CellStyle style = cell.getCellStyle();
				int alignment = style.getAlignment(); 
				switch (alignment) {
				case CellStyle.ALIGN_GENERAL:
					System.out.println("ALIGN_GENERAL");
					break;
				case CellStyle.ALIGN_LEFT:
					System.out.println("ALIGN_LEFT");
					break;
				case CellStyle.ALIGN_CENTER:
					System.out.println("ALIGN_CENTER");
					break;
				case CellStyle.ALIGN_RIGHT:
					System.out.println("ALIGN_RIGHT");
					break;
				case CellStyle.ALIGN_FILL:
					System.out.println("ALIGN_FILL");
					break;
				case CellStyle.ALIGN_JUSTIFY:
					System.out.println("ALIGN_JUSTIFY");
					break;
				case CellStyle.ALIGN_CENTER_SELECTION:
					System.out.println("ALIGN_CENTER_SELECTION");
					break;
				default:
					System.out.println("Error:Unknow Align Type");
					break;
				}
				
				String fmStr = style.getDataFormatString();
				System.out.println("Data format string:" + fmStr);
				
				double value = cell.getNumericCellValue();
				System.out.println(value);
			} catch (InvalidFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		case 8:
			// Use an InputStream, needs more memory
			try {
				wb = WorkbookFactory.create(new FileInputStream("MyExcel.xlsx"));
			} catch (InvalidFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 9:
			//If using HSSFWorkbook or XSSFWorkbook directly, you should generally go through NPOIFSFileSystem or OPCPackage, to have full control of the lifecycle (including closing the file when done):
			// HSSFWorkbook, File
			fs = new NPOIFSFileSystem(new File("file.xls"));
			//wb = new HSSFWorkbook(fs.getRoot());
			wb = new HSSFWorkbook(fs.getRoot(), false);
			fs.close();

			// HSSFWorkbook, InputStream, needs more memory
			FileInputStream myInputStream = new FileInputStream("workbook.xls");
			fs = new NPOIFSFileSystem(myInputStream);
			wb = new HSSFWorkbook(fs.getRoot(), false);
			break;
		case 10:
			/*
			// XSSFWorkbook, File
			OPCPackage pkg = OPCPackage.open(new File("file.xlsx"));
			XSSFWorkbook wb = new XSSFWorkbook(pkg);
			//....
			pkg.close();

			// XSSFWorkbook, InputStream, needs more memory
			OPCPackage pkg = OPCPackage.open(myInputStream);
			XSSFWorkbook wb = new XSSFWorkbook(pkg);
			//....
			pkg.close();
			*/
			break;
		case 11:
		      //wb = new XSSFWorkbook(); //or new HSSFWorkbook();
		      wb = new HSSFWorkbook(); //or new HSSFWorkbook();

		      sheet = wb.createSheet();
		      row = sheet.createRow((short) 2);
		      row.setHeightInPoints(30);

		      createCell(wb, row, (short) 0, CellStyle.ALIGN_CENTER, CellStyle.VERTICAL_BOTTOM);
		      createCell(wb, row, (short) 1, CellStyle.ALIGN_CENTER_SELECTION, CellStyle.VERTICAL_BOTTOM);
		      createCell(wb, row, (short) 2, CellStyle.ALIGN_FILL, CellStyle.VERTICAL_CENTER);
		      createCell(wb, row, (short) 3, CellStyle.ALIGN_GENERAL, CellStyle.VERTICAL_CENTER);
		      createCell(wb, row, (short) 4, CellStyle.ALIGN_JUSTIFY, CellStyle.VERTICAL_JUSTIFY);
		      createCell(wb, row, (short) 5, CellStyle.ALIGN_LEFT, CellStyle.VERTICAL_TOP);
		      createCell(wb, row, (short) 6, CellStyle.ALIGN_RIGHT, CellStyle.VERTICAL_TOP);

		        // Write the output to a file
		      fileOut = new FileOutputStream("hssf-align.xls");
		      wb.write(fileOut);
		      fileOut.close();
		      break;
		case 12:
			// TODO aaa
			try {
				wb = WorkbookFactory.create(new File("workbook.xls"));
				sheet = wb.getSheetAt(0);
				String sheetName = sheet.getSheetName();
				System.out.println(sheetName);
				row = sheet.getRow(2);
				cell = row.getCell(0);
				sheet = wb.getSheetAt(0);
				for (Row r : sheet) {
					for (Cell c : row) {
						getCellType(c);
						getAllignmentType(c);
					}
				}
			} catch (InvalidFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 13:
			int MY_MINIMUM_COLUMN_COUNT = 100;
			try {
				wb = WorkbookFactory.create(new File("workbook.xls"));
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			sheet = wb.getSheetAt(0);
			int rowStart = sheet.getFirstRowNum();
			int rowEnd = sheet.getLastRowNum();
			System.out.println("Row start=" + rowStart);
			System.out.println("Row End=" + rowEnd);
			String value = null;
		    // Decide which rows to process
		    //rowStart = Math.min(15, sheet.getFirstRowNum());
		    //rowEnd = Math.max(1400, sheet.getLastRowNum());

		    for (int rowNum = rowStart; rowNum <= rowEnd + 1; rowNum++) {
		       Row r = sheet.getRow(rowNum);

		       if (r == null)
		    	   continue;
		       //int lastColumn = Math.max(r.getLastCellNum(), MY_MINIMUM_COLUMN_COUNT);
		       int lastColumn = r.getLastCellNum();
		       System.out.printf("Last column = %d\n", lastColumn);

		       for (int cn = 0; cn < lastColumn; cn++) {
		          Cell c = r.getCell(cn, Row.RETURN_BLANK_AS_NULL);
		          System.out.printf("(%d,  %d) = ", rowNum, cn);
		          if (c == null) {
		        	  value = "null";
		          } else {
		        	  value = getCellValueStr(c);
		        	  printCellValue(c);
		          }
		          //System.out.println(value);
		       }
		    }
		    break;
		case 14:
			try {
				wb = WorkbookFactory.create(new File("workbook.xls"));
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			sheet = wb.getSheetAt(0);
			row = sheet.getRow(0);
			cell = row.getCell(CellReference.convertColStringToIndex("B"));
			System.out.printf("%s\n", cell.getStringCellValue());
			CellReference cellRef = new CellReference("A3");
			int r = cellRef.getRow();
			int c = cellRef.getCol();
			System.out.printf("(%d, %d)\n", r, c);
			System.out.printf("Index of column B = %d", cellRef.convertColStringToIndex("B"));
			break;
		case 15:
		    wb = new HSSFWorkbook();
		    sheet = wb.createSheet("format sheet");
		    CellStyle style;
		    DataFormat format = wb.createDataFormat();
		    short rowNum = 0;
		    short colNum = 0;

		    row = sheet.createRow(rowNum++);
		    cell = row.createCell(colNum);
		    cell.setCellValue(11111.25);
		    style = wb.createCellStyle();
		    style.setDataFormat(format.getFormat("0.0"));
		    cell.setCellStyle(style);

		    row = sheet.createRow(rowNum++);
		    cell = row.createCell(colNum);
		    cell.setCellValue(11111.25);
		    style = wb.createCellStyle();
		    style.setDataFormat(format.getFormat("#,##0.0000"));
		    cell.setCellStyle(style);

		    fileOut = new FileOutputStream("workbook.xls");
		    wb.write(fileOut);
		    fileOut.close();
		    break;
		case 16:
			try {
				wb = WorkbookFactory.create(new File("c:/TEMP/SCEC.xlsx"));
				//wb = WorkbookFactory.create(new File("/Users/MBP/Project/SCEC/職能分析表.xlsx"));
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			//Get number of sheet
			int numOfSheet = wb.getNumberOfSheets();
			String sName = null;
			for (int ns = 0; ns < numOfSheet; ns++) {
				sheet = wb.getSheetAt(ns);
				sName = sheet.getSheetName();
				if (sName.equals("組織架構")) {
					rowStart = sheet.getFirstRowNum();
					rowEnd = sheet.getLastRowNum();
					System.out.println("Row start=" + rowStart);
					System.out.println("Row End=" + rowEnd);
					value = null;
				    // Decide which rows to process
				    //rowStart = Math.min(15, sheet.getFirstRowNum());
				    //rowEnd = Math.max(1400, sheet.getLastRowNum());
		
					//Check header
				    Row r1 = sheet.getRow(rowStart);
				    int firstCol = r1.getFirstCellNum();
				    int lastCol = r1.getLastCellNum();
				    int colIdx = 0, currentCol = 0;
				    for (colIdx = firstCol; colIdx < lastCol; colIdx++) {
				    	Cell c1 = r1.getCell(colIdx, Row.RETURN_BLANK_AS_NULL);
				        System.out.printf("\n(%d,%d)", rowStart, colIdx);
				        if (c1 == null) {
				        	continue;
				        }
				        
				        value = getCellValueStr(c1);
				        printCellValue(c1);
				        System.out.println();
				        currentCol = c1.getColumnIndex();
				        System.out.printf("Current column=(%d,%d)", colIdx, currentCol);
				        if (value.equalsIgnoreCase("部門")) {
				        	readColumn(sheet, colIdx);
				        } else if (value.equalsIgnoreCase("職系")) {
				        	readColumn(sheet, colIdx);
				        } else if (value.equalsIgnoreCase("職等")) {
				        	readColumn(sheet, colIdx);
				        } else if (value.equalsIgnoreCase("職稱")) {
				        	readColumn(sheet, colIdx);
				        } else if (value.equals("職能類別")) {
				        	readColumn(sheet, colIdx);
				        } else if (value.equalsIgnoreCase("職能項目")) {
				        	readColumn(sheet, colIdx);
				        } else if (value.equalsIgnoreCase("科目")) {
				        	readColumn(sheet, colIdx);
				        }
				        //TODO: need check if all columns are found, if false then break and issue error
				        
				    }
				    
				    for (int rowNum1 = rowStart; rowNum1 <= rowEnd + 1; rowNum1++) {
				       r1 = sheet.getRow(rowNum1);
		
				       if (r1 == null)
				    	   continue;
				       //int lastColumn = Math.max(r.getLastCellNum(), MY_MINIMUM_COLUMN_COUNT);
				       firstCol = r1.getFirstCellNum();
				       lastCol = r1.getLastCellNum();
				       System.out.printf("\nFirst column = %d, Last column = %d\n", firstCol, lastCol);
		
				       for (int cn = firstCol; cn < lastCol; cn++) {
				          Cell c1 = r1.getCell(cn, Row.RETURN_BLANK_AS_NULL);
				          System.out.printf("(%d,  %d) = ", rowNum1, cn);
				          if (c1 == null) {
				        	  value = "null";
				          } else {
				        	  value = getCellValueStr(c1);
				        	  printCellValue(c1);
				          }
				          //System.out.println(value);
				       }
				    }
				}
			}
		    break;
		}
	}

	private static void readColumn(Sheet sheet, int colIdx) {
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		Row row = null;
		Cell cell = null;
		
		for (int r = firstRow + 1; r < lastRow; r++) {
			row = sheet.getRow(r);
			cell = row.getCell(colIdx, Row.RETURN_NULL_AND_BLANK);
			if (cell == null)
				continue;
			
			printCellValue(cell);
		}
	}
	
    /**
     * Creates a cell and aligns it a certain way.
     *
     * @param wb     the workbook
     * @param row    the row to create the cell in
     * @param column the column number to create the cell in
     * @param halign the horizontal alignment for the cell.
     */
    private static void createCell(Workbook wb, Row row, short column, short halign, short valign) {
        Cell cell = row.createCell(column);
        cell.setCellValue("Align It");
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(halign);
        cellStyle.setVerticalAlignment(valign);
        cell.setCellStyle(cellStyle);
    }
    
    private static short getAllignmentType(Cell cell) {
    	CellStyle style = cell.getCellStyle();
    	short type = style.getAlignment();
    	switch(type) {
		case CellStyle.ALIGN_GENERAL:
			System.out.println("ALIGN_GENERAL");
			break;
		case CellStyle.ALIGN_LEFT:
			System.out.println("ALIGN_LEFT");
			break;
		case CellStyle.ALIGN_CENTER:
			System.out.println("ALIGN_CENTER");
			break;
		case CellStyle.ALIGN_RIGHT:
			System.out.println("ALIGN_RIGHT");
			break;
		case CellStyle.ALIGN_FILL:
			System.out.println("ALIGN_FILL");
			break;
		case CellStyle.ALIGN_JUSTIFY:
			System.out.println("ALIGN_JUSTIFY");
			break;
		case CellStyle.ALIGN_CENTER_SELECTION:
			System.out.println("ALIGN_CENTER_SELECTION");
			break;
		default:
			System.out.println("Error:Unknow Align Type");
			type = -1;
			break;
		}
    	
    	return type;
    }
    
    private static int getCellType(Cell cell) {
    	int type = cell.getCellType();
    	switch(type) {
		case Cell.CELL_TYPE_BLANK:
			System.out.println("ALIGN_GENERAL");
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			System.out.println("ALIGN_LEFT");
			break;
		case Cell.CELL_TYPE_ERROR:
			System.out.println("ALIGN_CENTER");
			break;
		case Cell.CELL_TYPE_FORMULA:
			System.out.println("ALIGN_RIGHT");
			break;
		case Cell.CELL_TYPE_NUMERIC:
			System.out.println("ALIGN_FILL");
			break;
		case Cell.CELL_TYPE_STRING:
			System.out.println("ALIGN_JUSTIFY");
			break;
		default:
			System.out.println("Error:Unknow Cell Type");
			type = -1;
			break;
		}
    	
    	return type;
    }
    
    private static String getCellValueStr(Cell cell) {
    	int type = cell.getCellType();
    	String value = null;
    	
    	switch(type) {
		case Cell.CELL_TYPE_BLANK:
			value = "Blank";
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			Boolean boVal = cell.getBooleanCellValue();
			value = boVal.toString();
			break;
		case Cell.CELL_TYPE_ERROR:
			Byte btVal = cell.getErrorCellValue();
			value = btVal.toString();
			break;
		case Cell.CELL_TYPE_FORMULA:
			value = cell.getCellFormula();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			Double dVal = cell.getNumericCellValue();
			value = dVal.toString();
			break;
		case Cell.CELL_TYPE_STRING:
			value = cell.getStringCellValue();
			break;
		default:
			type = -1;
			break;
		}
		//System.out.printf("Value = %s\n", value);
    	
    	return value;
    }
    
    private static void printCellValue(Cell cell) {
    	int type = cell.getCellType();
    	
        CellReference cellRef = new CellReference(cell.getRowIndex(), cell.getColumnIndex());
        System.out.print(cellRef.formatAsString());
        System.out.print("=");

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                System.out.printf("%s", cell.getRichStringCellValue().getString());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    System.out.printf("%s", cell.getDateCellValue());
                } else {
                    System.out.printf("%s", cell.getNumericCellValue());
                }
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                System.out.printf("%s", cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA:
                System.out.printf("%s", cell.getCellFormula());
                break;
            default:
                System.out.printf("Error:unknow cell value type");
        }
    }
}
