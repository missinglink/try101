package try101.se.generic;

import java.util.*;

public class GenericFoo<T extends List> {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GenericFoo<ArrayList> foo = null;
		//GenericFoo<HashMap> foo2 = null; error
		GenericFoo<? extends List> foo3 = null; 
		//GenericFoo0<? extends List> foo4 = null; Error
		
		//foo3 = new GenericFoo<HashMap>();
	}

}
