package try101.sort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Date;

public class MapSort {
	
	public static void main(String [] args) throws IOException {
		
		Comparator<Map<String, Integer>> comparator = new Comparator<Map<String, Integer>>() {
			//建立新的Comparator class。可以另外建立class然後宣告import，
			//但我比較懶...直接在同一JAVA裡建匿名class
			String sortBy = "hp";

			public int compare(Map<String, Integer> o1, Map<String, Integer> o2) {
				//override compare function

				return o1.get(sortBy).compareTo(o2.get(sortBy));
				//調用Integer的compareTo方法，這樣排出來是遞增(ASC)
				//想要遞減(DESC)排的話就把object1.2的順序互換↓
				//o2.get(sortBy).compareTo(o1.get(sortBy));
			}
		};

		Comparator<Map.Entry<String, Date>> comparator2 = new Comparator<Map.Entry<String, Date>>() {

			public int compare(Map.Entry<String, Date> o1, Map.Entry<String, Date> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		};

		Comparator<Map.Entry<String, Date>> comparator3 = new Comparator<Map.Entry<String, Date>>() {

			public int compare(Map.Entry<String, Date> o1, Map.Entry<String, Date> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		};

		Map<String, Integer> map1 = new HashMap<String, Integer>();
		Map<String, Integer> map2 = new HashMap<String, Integer>();
		Map<String, Integer> map3 = new HashMap<String, Integer>();
		List<Map<String,Integer>> list =  new ArrayList<Map<String, Integer>>();
		
		List<Map.Entry<String, Date>> listd = null;
		Map<String, Date> mapd = new HashMap<String, Date>();
		Date date = null;
		
		
		int ex = 3;
		
		switch (ex) {
		case 1:
			map1.put("hp", 8); 
			map1.put("atk", 9); 
			map1.put("def", 9); 
			map2.put("hp", 10); 
			map2.put("atk", 8); 
			map2.put("def", 7); 
			map3.put("hp", 8); 
			map3.put("atk", 7); 
			map3.put("def", 10); 
			
			list.add(map1);
			list.add(map2);
			list.add(map3);
			
			System.out.printf("%s\n", list);
			Collections.sort(list, comparator);
			System.out.printf("%s\n", list);
			break;
		case 2:
			date = new Date();
			mapd.put("title1", date);
			date = new Date();
			mapd.put("title3", date);
			date = new Date();
			mapd.put("title2", date);
			listd = new ArrayList<Map.Entry<String, Date>>(mapd.entrySet());
			System.out.printf("%s\n", mapd);
			Collections.sort(listd, comparator2);
			System.out.printf("%s\n", mapd);
			
			break;
		case 3:
			date = new Date();
			mapd.put("title1", date);
			date = new Date();
			mapd.put("title3", date);
			date = new Date();
			mapd.put("title2", date);
			listd = new ArrayList<Map.Entry<String, Date>>(mapd.entrySet());
			System.out.printf("%s\n", mapd);
			System.out.printf("%s\n", listd);
			Collections.sort(listd, comparator3);
			System.out.printf("%s\n", mapd);
			System.out.printf("%s\n", listd);
			
			break;
		}
	}

}
