/*
 *  1. 萬用複合查詢-可由客戶端隨意增減任何想查詢的欄位
 *  2. 為了避免影響效能:
 *        所以動態產生萬用SQL的部份,本範例無意採用MetaData的方式,也只針對個別的Table自行視需要而個別製作之
 * */

package try101.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;


public class CompositeQuery implements QueryComposable {
	// private String whereString;
	// private Map<String, String> parametersMap;
	//
	// public String getWhereString() {
	// return whereString;
	// }
	//
	// public Map<String, String> getParametersMap() {
	// return parametersMap;
	// }

	@Override
	public Map<String, String> composeQuery(Map<String, String[]> map) {

		Map<String, String> result = new HashMap<String, String>();

		// 逐個欄位處理
		Set<String> keys = map.keySet();
		StringBuffer whereCondition = new StringBuffer();
		whereCondition.append("");
		int count = 0; // 欄位數
		int parameterCounter = 0;
		for (String key : keys) {
			String value = map.get(key)[0];
			if (value != null && value.trim().length() != 0) {
				String[] aCondition = getConditionString(key, value.trim());

				// aCondition[0] 放資料類型: 目前分三種: null 代表非條件, ""無條件值,
				// int, Date, 及 String 代表有條件值
				// aCondition[1] 若為 null, 代表條件值格式不對
				// aCondition[2] 存產生的條件, 以 ? 當參數. 若 aCondition[1] 是 null, 此處應該是
				// 1 = 0
				if (aCondition[0] != null && !"".equals(aCondition[0])) {
					count++;
					if (count == 1) {
						whereCondition.append(" where " + aCondition[2]);
					} else {
						whereCondition.append(" and " + aCondition[2]);
					}

					if (aCondition[1] != null) {
						parameterCounter++;
						result.put("Parameter:" + aCondition[0] + ":"
								+ parameterCounter, value.trim());
					}
					// System.out.println("有送出查詢資料的欄位數count = " + count);
				}
				// 不是想要處理的條件
			}
		}
		result.put("QueryString", whereCondition.toString());
//		for (String key : result.keySet()) {
//			System.out.println("***key: " + key + " ***value:"
//					+ result.get(key) + "**");
//		}

//		 System.out.println("where:" + whereCondition.toString());
		// System.out.println("parameters: " + (parametersMap == null ? 0 :
		// parametersMap.size()));

		return result;
	}

	/**
	 * 將所要傳入 query 參數 bind 進入 參數有順序性, 需與 composeQuery 配合
	 */
	@Override
	public boolean bindParameters(PreparedStatement pstmt,
			Map<String, String> parametersMap) {
		int count = 0;
		try {
			for (String keyStr : parametersMap.keySet()) { // 逐個欄位 bind
				if (!keyStr.startsWith("Parameter:")) {
					continue;
				}
				// 處理參數: 0 是 Parameter, 1: 參數類型; 2 是 第幾個參數
				String key = keyStr.split(":")[1];
				String countStr = keyStr.split(":")[2];
				count = Integer.parseInt(countStr);
				if ("int".equals(key)) {
					pstmt.setInt(count,
							Integer.valueOf(parametersMap.get(keyStr)));
				} else if ("Date".equals(key)) {
					java.sql.Date date = java.sql.Date.valueOf(parametersMap
							.get(keyStr));
					pstmt.setDate(count, date);
				} else {
					pstmt.setString(count, parametersMap.get(keyStr));
				}
			}
		} catch (NumberFormatException | SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("***** 參數綁定錯誤: " + e.getMessage() + " count:"
					+ count);
			return false;
		}
		return true;
	}

	@Override
	public String[] getConditionString(String columnName, String value) {
		// TODO Auto-generated method stub
		return null;
	}

}
