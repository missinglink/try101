package try101.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBLibrary {

	/**
	 * Closes the current statement
	 * 
	 * @param ps
	 *            Statement
	 */
	public static void close(Statement pstmt) {
		if (pstmt != null) {
			try {
				pstmt.close();
			} catch (SQLException se) {
				se.printStackTrace(System.err);
			}
		}
	}

	/**
	 * Closes the current resultset
	 * 
	 * @param ps
	 *            Statement
	 */
	public static void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException se) {
				se.printStackTrace(System.err);
			}
		}
	}

	/**
	 * Closes the current statement
	 * 
	 * @param ps
	 *            Statement
	 */
	public static void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (Exception e) {
				e.printStackTrace(System.err);
			}
		}
	}

}
