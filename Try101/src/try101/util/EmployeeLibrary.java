package try101.util;

import org.apache.commons.lang3.RandomStringUtils;

import com.employee.model.EmployeeVO;

public class EmployeeLibrary {

	private final static String EMPLOYEE_PASSWORD_SUBJECT = "密碼變更通知";
	private final static int LENGTH_OF_INITIAL_PASSWORD = 8;

	// 產生一個 8 碼的亂數當作密碼
	public static String getInitialPasswordForEmployee() {
		return RandomStringUtils.random(LENGTH_OF_INITIAL_PASSWORD, true, true);
	}

	// throws AddressException, MessagingException
	public static void sendEmailToEmployee(EmployeeVO employeeVO
										 , String emp_password, String message)  {

		String mailTo = employeeVO.getEmp_email();   //"jj.james.hsu@gmail.com";
		String bodyMessage = createEmployeePasswordNotification(
				employeeVO, emp_password, message);
		String[] fileAttachment = null; 

		try {
			GeneralLibrary.sendEmail(mailTo, EMPLOYEE_PASSWORD_SUBJECT, bodyMessage, fileAttachment);
			System.out.println("Email 已送出給 " + employeeVO.getEmp_no()
					+ "  " + employeeVO.getEmp_name()
					+ " password:" + emp_password);
		} catch (Exception e) {
			System.out.println("Email 發送失敗: " + e.getMessage());
		}
	}

	// 產生員工密碼通知函內容
	private static String createEmployeePasswordNotification(EmployeeVO employeeVO, 
													String emp_password, String message) {
		StringBuilder sb = new StringBuilder(1024);
		sb.append("<!DOCTYPE html>");
		sb.append("<html lang=\"en\">");
		sb.append("<head>");
//		sb.append("<meta contentType=\"text/html; charset=UTF-8\" ");
		sb.append("<title>員工密碼通知函</title>");
		sb.append("</head>");
		sb.append("<body>");
		sb.append(employeeVO.getEmp_name());
		sb.append(" 您好，<br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
		sb.append(message);
		sb.append("，您的帳號是您的員工編號 ");
		sb.append(employeeVO.getEmp_no());
		sb.append("， 密碼是 " + emp_password);
		sb.append("， 請您儘快上系統變更您的密碼。<br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
//		sb.append("系統網址為  <a href=\"http://localhost:8081/WA1021G1/backend/login.jsp\">售票管理系統入口</a><br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;謝謝。<br><br><br>");
		sb.append("*** 此信件係自動發送，請不要回覆。");	    
		sb.append("</body>");
		sb.append("</html>");

		return sb.toString(); 
	}
	
}
