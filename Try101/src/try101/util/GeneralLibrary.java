package try101.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.text.*;
import java.util.Date;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import org.apache.commons.lang3.RandomStringUtils;

public class GeneralLibrary {

	private final static int ITERATION_NUMBER = 2;
	private final static String EMAIL_FORMAT = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
	
//	private final static String EMAIL_FORMAT = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private final static String PASSWORD_FORMAT = "([a-zA-Z0-9]){3,12}";
	private final static String PHONE_FORMAT = "^0(\\d){1,3}-?(\\d){6,8}";

	// for emial
	private final static String SMTP = "smtp.gmail.com";
	private final static String SMTP_PORT = "587"; // 或 993 或 465
	private final static String MAIL_SENDER = "wa102g1.iii@gmail.com";
	private final static String MAIL_PASSWORD = "wa102g1III";
	private final static String MAIL_NO_REPLY = "no-reply.wa102g1@gmail.com";

	public static boolean isValidPhoneNumber(String phone) {
		return phone.matches(PHONE_FORMAT);
	}

	public static boolean isValidEmail(String email) {
		return email.matches(EMAIL_FORMAT);
	}

	// throws AddressException, MessagingException
	public static void sendEmail(String mailTo, String subject
							   , String bodyMessage, String[] fileAttachment)  {

		String host = SMTP;
		String port = SMTP_PORT;
		String mailFrom = MAIL_SENDER;
		String password = MAIL_PASSWORD;

		try {
			EmailSender sender = new EmailSender();
			sender.sendEmail(host, port, mailFrom, password, mailTo, subject,
					bodyMessage, fileAttachment, MAIL_NO_REPLY);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public static boolean isValidPassword(String password) {
		return password.matches(PASSWORD_FORMAT);
	}

	/**
	 * A simplified algorithm based on the code of Owasp project The salt is the
	 * same for every employees
	 * 
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * */
	public static String encrypt(String password, String salt) {
		if (password == null) {
			return null;
		}
		//
		byte[] bSalt = null;
		try {
			bSalt = base64ToByte(salt);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Compute the new DIGEST
		return byteToBase64(getHash(ITERATION_NUMBER, password, bSalt));

	}

	/**
	 * The encryption program comes from the website:
	 * https://www.owasp.org/index.php/Hashing_Java It is part of OWASP which
	 * means The Open Web Application Security Project. We use only part of the
	 * methods in our project. People who are interested in the project are
	 * encouraged to visit its website https://www.owasp.org/index.php.
	 * 
	 * @throws NoSuchAlgorithmException
	 */
	public static byte[] getHash(int iterationNb, String password, byte[] salt) {
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		digest.reset();
		digest.update(salt);
		byte[] input = null;
		try {
			input = digest.digest(password.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i < iterationNb; i++) {
			digest.reset();
			input = digest.digest(input);
		}
		return input;
	}

	/**
	 * From a base 64 representation, returns the corresponding byte[]
	 * 
	 * @param data
	 *            String The base64 representation
	 * @return byte[]
	 * @throws IOException
	 */
	public static byte[] base64ToByte(String data) throws IOException {
		BASE64Decoder decoder = new BASE64Decoder();
		return decoder.decodeBuffer(data);
	}

	/**
	 * From a byte[] returns a base 64 representation
	 * 
	 * @param data
	 *            byte[]
	 * @return String
	 * @throws IOException
	 */
	public static String byteToBase64(byte[] data) {
		BASE64Encoder endecoder = new BASE64Encoder();
		return endecoder.encode(data);
	}
	
	//  判斷是否是合理的日期 格式
	public static boolean isValidDateFormat(String inputStr) {
		if (inputStr == null || inputStr.trim().length() != 10) return false;
		ParsePosition position = new ParsePosition(0);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = formatter.parse(inputStr, position);
//		if (date == null) System.out.println("null");
		return (formatter.parse(inputStr, position) == null); 
	}
	
	public static String getCurrentTimestampString() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		return df.format(new java.util.Date());		
	}
	
	
}
