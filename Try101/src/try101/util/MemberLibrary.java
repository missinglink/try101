package try101.util;

import org.apache.commons.lang3.RandomStringUtils;

import com.member.model.MemberVO;

public class MemberLibrary {

	private final static int LENGTH_OF_INITIAL_PASSWORD = 8;
	private final static String MEMBER_PASSWORD_SUBJECT = "會員密碼變更通知";
	private final static String NEW_MEMBER_SUBJECT = "感謝 您加入 EZTicket 輕鬆購票系統";

	// 產生一個 8 碼的亂數當作密碼
	public static String getInitialPasswordForMember() {
		return RandomStringUtils.random(LENGTH_OF_INITIAL_PASSWORD, true, true);
	}

	// throws AddressException, MessagingException
	public static void sendEmailToMember(MemberVO memberVO
										 , String member_password, String message)  {

		String mailTo = memberVO.getMember_id();   //"jj.james.hsu@gmail.com";
		String bodyMessage = createMemberPasswordNotification(
				memberVO, member_password, message);
		String[] fileAttachment = null; 

		try {
			GeneralLibrary.sendEmail(mailTo, MEMBER_PASSWORD_SUBJECT, bodyMessage, fileAttachment);
			System.out.println("Email 已送出給 " + memberVO.getMember_id()
					+ "  " + memberVO.getMember_id()
					+ " password:" + member_password);
		} catch (Exception e) {
			System.out.println("Email 發送失敗: " + e.getMessage());
		}
	}

	// 產生員工密碼通知函內容
	private static String createMemberPasswordNotification(MemberVO memberVO, 
										String member_password, String message) {
		StringBuilder sb = new StringBuilder(1024);
		sb.append("<!DOCTYPE html>");
		sb.append("<html lang=\"en\">");
		sb.append("<head>");
//		sb.append("<meta contentType=\"text/html; charset=UTF-8\" ");
		sb.append("<title>會員密碼通知函</title>");
		sb.append("</head>");
		sb.append("<body>");
		sb.append(memberVO.getMember_name());
		String sex = ("M".equals(memberVO.getMember_sex()) ? " 先生" 
				   : ("F".equals(memberVO.getMember_sex()) ? " 小姐" : ""));
		sb.append(sex);
		sb.append(" 您好，<br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
		sb.append(message);
		sb.append("，新的 密碼是 " + member_password);
		sb.append("， 請您儘快上系統變更您的密碼。<br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
//		sb.append("系統網址為  <a href=\"http://localhost:8081/WA1021G1/member/changePassword.jsp\">變更密碼</a><br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;謝謝。<br><br><br>");
		sb.append("*** 此信件係自動發送，請不要回覆。");	    
		sb.append("</body>");
		sb.append("</html>");

		return sb.toString(); 
	}
	
	// throws AddressException, MessagingException
	public static void newMemberNotofication(MemberVO memberVO, String member_password)  {

		String mailTo = memberVO.getMember_id();   //"jj.james.hsu@gmail.com";
		String bodyMessage = createNewMemberNotification(memberVO, member_password);
		String[] fileAttachment = null; 

		try {
			GeneralLibrary.sendEmail(mailTo, NEW_MEMBER_SUBJECT, bodyMessage, fileAttachment);
			System.out.println("Email 已送出給 " + memberVO.getMember_id()
					+ "  " + memberVO.getMember_id());
		} catch (Exception e) {
			System.out.println("Email 發送失敗: " + e.getMessage());
		}
	}

	// 產生員工密碼通知函內容
	private static String createNewMemberNotification(MemberVO memberVO
													, String member_password) {
		StringBuilder sb = new StringBuilder(1024);
		sb.append("<!DOCTYPE html>");
		sb.append("<html lang=\"en\">");
		sb.append("<head>");
//		sb.append("<meta contentType=\"text/html; charset=UTF-8\" ");
		sb.append("<title>" + NEW_MEMBER_SUBJECT + "</title>");
		sb.append("</head>");
		sb.append("<body>");
		sb.append(memberVO.getMember_name());
		String sex = ("M".equals(memberVO.getMember_sex()) ? " 先生" 
				   : ("F".equals(memberVO.getMember_sex()) ? " 小姐" : ""));
		sb.append(sex);
		sb.append(" 您好，<br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
		sb.append(NEW_MEMBER_SUBJECT);
		sb.append("， 您的帳號是 " + memberVO.getMember_id());
		sb.append("，密碼是 " + member_password);
		sb.append("， 您可立即上系統進行活動的購票。<br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
//		sb.append("系統網址為  <a href=\"http://localhost:8081/WA1021G1/index.jsp\">EZTicket 輕鬆購票系統</a><br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;謝謝。<br><br><br>");
		sb.append("*** 此信件係自動發送，請不要回覆。");	    
		sb.append("</body>");
		sb.append("</html>");

		return sb.toString(); 
	}
	
}
