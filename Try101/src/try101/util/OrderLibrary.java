package try101.util;

import org.apache.commons.lang3.RandomStringUtils;

import com.member.model.MemberVO;
import com.orderdetail.model.TicketDetailVO;
import com.ordermaster.model.TicketMasterVO;
import com.sms.Send;

public class OrderLibrary {

	private final static String NEW_ORDER_SUBJECT = "訂單成立通知";
	private final static int LENGTH_OF_VERIFICATION_CODE = 8;
	
	// 產生一個 8 碼的亂數當作訂單驗證碼
	public static String getOrderVerificationCode() {
		return RandomStringUtils.random(LENGTH_OF_VERIFICATION_CODE, true, true);
	}

	// throws AddressException, MessagingException
	public static void sendOrderEmail(MemberVO memberVO
			                        , TicketMasterVO ticketMasterVO) {

		String mailTo = memberVO.getMember_id();   //"jj.james.hsu@gmail.com";
		String bodyMessage = createOrderNotification(memberVO, ticketMasterVO);
		String[] fileAttachment = null; 

		try {
			GeneralLibrary.sendEmail(mailTo, NEW_ORDER_SUBJECT, bodyMessage, fileAttachment);
			System.out.println("Email 已送出給 " + memberVO.getMember_id()
					+ "  " + memberVO.getMember_name());
		} catch (Exception e) {
			System.out.println("Email 發送失敗: " + e.getMessage());
		}
	}

	// 產生訂單通知內容
	private static String createOrderNotification(MemberVO memberVO
												, TicketMasterVO ticketMasterVO) {
		StringBuilder sb = new StringBuilder(1024);
		sb.append("<!DOCTYPE html>");
		sb.append("<html lang=\"en\">");
		sb.append("<head>");
//		sb.append("<meta contentType=\"text/html; charset=UTF-8\" ");
		sb.append("<title>EZTicket 輕鬆購票系統 - 謝謝 您的訂購</title>");
		sb.append("</head>");
		sb.append("<body>");
		sb.append(memberVO.getMember_name());
		String sex = ("M".equals(memberVO.getMember_sex()) ? " 先生" 
				   : ("F".equals(memberVO.getMember_sex()) ? " 小姐" : ""));
		sb.append(sex);
		sb.append(" 您好，<br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
		sb.append("感謝您使用本系統訂購活動的票 ，以下是 您的訂單:<br>");
		sb.append("----------------------------------------------<br>");
		sb.append("訂單編號: " + ticketMasterVO.getTkm_seq_no() + "<br>");
		sb.append("訂單日期: " + ticketMasterVO.getTkm_date() + "<br>");
		sb.append("訂單金額: " + ticketMasterVO.getTkm_amount() + "<br>");
		sb.append("驗證碼: " + ticketMasterVO.getTkm_verification_code() + "<br><br>");
		sb.append("   ------------------ 訂單明細 -------------<br><br>");
		sb.append("<table  style=\"border: 1px solid grey; border-spacing:2px\">");
		sb.append("<tr><th>活動及座位資訊<th>金額</th></tr>");
		for (TicketDetailVO tdVO : ticketMasterVO.getTicketDetail()) {
			sb.append("<tr><td style=\"text-align:left; vertical-align:top; border: 1px solid grey; border-spacing:2px\">"
		            + tdVO.getTkd_description().replace("\\n", "<br/>") + "</td>"
					+ "<td style=\"text-align:left; vertical-align:top; border: 1px solid grey; border-spacing:2px\">"
		            + tdVO.getTkd_price() + "</td></tr>");
		}
		sb.append("</table>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
//		sb.append("系統網址為  <a href=\"http://localhost:8081/WA1021G1/member/memlogin.jsp\">會員登入</a><br><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;謝謝。<br><br><br>");
		sb.append("*** 此信件係自動發送，請不要回覆。");	    
		sb.append("</body>");
		sb.append("</html>");

		return sb.toString(); 
	}
	
	public static void sendOrderShortMessage(String cellphone
									, TicketMasterVO ticketMasterVO) {
		
		String[] member_tel = new String[1];
		member_tel[0] = cellphone.replace("-",  "");
	 	Send se = new Send();
	 	
	 	String message = "EZTicket 輕鬆購票系統感謝 您的訂購，您的訂單編號為 " 
	 	               + ticketMasterVO.getTkm_seq_no() 
	 	               + " 驗證碼是 " + ticketMasterVO.getTkm_verification_code();
	 	se.sendMessage(member_tel , message);

	}
}
