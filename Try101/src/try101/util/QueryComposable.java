package try101.util;

import java.sql.PreparedStatement;
import java.util.*;

// 定義複合查詢所需的介面
public interface QueryComposable {

	// 自 request 中取得查詢的資料
	public Map<String, String> composeQuery(Map <String, String[]> map);
	public boolean bindParameters(PreparedStatement pstmt, Map<String, String> parametersMap);
	public String[] getConditionString(String columnName, String value);
}
