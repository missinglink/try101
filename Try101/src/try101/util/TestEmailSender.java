package try101.util;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
 
/*
 * Google gmail host : smtp.gmail.com
 * Port:
 * 587 for TLS/STARTTLS
 * 465 for SSL
 * 
 */
public class TestEmailSender {
    public static void main(String[] args) throws AddressException, MessagingException {
        String host = "smtp.gmail.com";
        String port = "587";    // 或 993 或 465
        //String port = "465";    // 或 993 或 465
        String mailFrom = "xxx@gmail.com";
        String mailTo = "xxx@gmail.com";
        String password = "xxx";
        String subject = "員工密碼確認函";
        String bodyMessage = "<html><b>Hello</b><br/><i>This is an HTML email with an attachment</i></html>";
        
        EmailSender sender = new EmailSender();
        String[] fileAttachment = null;   // {"E:/Freelancer/RentACoder/Bids/Forum/questions.txt"};
        sender.sendEmail(host, port, mailFrom, password, mailTo, subject, bodyMessage, fileAttachment,"no-reply.missing.link.hsing1@gmail.com");       
    }
}
